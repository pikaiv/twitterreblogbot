<?php

$opt = getopt('q');

require_once("settings.php");
$rss = new SimpleXMLElement("https://twitter-great-rss.herokuapp.com/feed/list?name={$list_account}&slug={$list_name}&url_id_hash={$id_hash}", 0, TRUE);

$dsn = "pgsql:host={$db_host};port={$db_port};dbname={$db_name}";
try {
	$pdo = new PDO($dsn, $db_user, $db_password);
}
catch ( PDOException $e ) {
	echo "接続失敗" . PHP_EOL;
	echo $e->errorInfo . PHP_EOL;
	echo $e->getMessage() . PHP_EOL;
	exit(1);
}
catch ( Exception $e ) {
	echo "なんかよーわからんエラー" . PHP_EOL;
	exit(1);
}

if ( !isset($opt['q']) ) {
	require_once("MastodonClient/MastodonClient.php");
	$mc = new MastodonClient();
	$mc->init();
}

$new_tweets = array();

try {
	foreach ( $rss->channel->item as $tweetnum => $tweetobject ) {
		list(,,,$tweet_screen_name,,$status_id) = explode('/', $tweetobject->link);
		$tweettime_datetime = DateTime::createFromFormat("D M j H:i:s O Y", $tweetobject->pubDate, new DateTimeZone("UTC"));
		$tweettime_datetime->setTimezone(new DateTimeZone("Asia/Tokyo")); 

		// すでに処理済のツイートではないかチェック
		$select_tweet_query = 'SELECT COUNT(*) FROM toots WHERE status_id = :status_id AND pubDate = :pubDate';
		if ( !isset($select_tweet_sth) ) {
			$select_tweet_sth = $pdo->prepare($select_tweet_query);
		}
		$select_tweet_sth->bindValue(':status_id', $status_id, PDO::PARAM_INT);
		$select_tweet_sth->bindValue(':pubDate', $tweettime_datetime->format('Y-m-d H:i:s'), PDO::PARAM_STR);
		$select_tweet_sth->execute();
		$result = $select_tweet_sth->fetch();
		if ( $result[0] > 0 ) {
			fprintf(STDERR, "スキップしたよ!".PHP_EOL);
			continue;
		}

		$insert_new_tweet_query = 'INSERT INTO toots (status_id, pubDate) VALUES (:status_id, :pubDate);';
		if ( !isset($insert_new_tweet_sth) ) {
			$insert_new_tweet_sth = $pdo->prepare($insert_new_tweet_query);
		}
		$insert_new_tweet_sth->bindValue(':status_id', $status_id, PDO::PARAM_INT);
		$insert_new_tweet_sth->bindValue(':pubDate', $tweettime_datetime->format('Y-m-d H:i:s'), PDO::PARAM_STR);
		$insert_new_tweet_sth->execute();
		if ( $insert_new_tweet_sth->rowCount() == 0 ) {
			echo "スキップしたよ！";
			continue;
		}

		$toot_text = "";
		$toot_text .= (($tweet_screen_name != $tweetobject->title) ? ":retweet:" : "🐦") . " ";
		$toot_text .= '['. $tweettime_datetime->format('Y-m-d H:i:s') .']'. PHP_EOL;
		$toot_text .= $tweetobject->title . " さんの". (($tweet_screen_name != $tweetobject->title) ? "リ" : "") . "ツイートです。" . PHP_EOL;
		$toot_text .= $tweetobject->link;

		array_unshift($new_tweets, array($status_id, $tweettime_datetime, $toot_text));
	}

	foreach ( $new_tweets as $new_tweet ) {
		if ( !isset($opt['q']) ) {
			// 失敗する可能性もなくはないけど、成功したことにする
			$mc->post_statuses(MastodonClient::VISIBILITY_UNLISTED, $new_tweet[2]);
		}
		$toot_at_datetime = new DateTime('now', new DateTimeZone('Asia/Tokyo'));

		$update_new_tweet_query = 'UPDATE toots SET toot_at = :toot_datetime WHERE status_id = :status_id AND pubDate = :pubDate';
		if ( !isset($update_new_tweet_sth) ) {
			$update_new_tweet_sth = $pdo->prepare($update_new_tweet_query);
		}
		$update_new_tweet_sth->bindValue(':status_id', $new_tweet[0], PDO::PARAM_INT);
		$update_new_tweet_sth->bindValue(':pubDate', $new_tweet[1]->format('Y-m-d H:i:s'), PDO::PARAM_STR);
		$update_new_tweet_sth->bindValue(':toot_datetime', $toot_at_datetime->format('Y-m-d H:i:s'), PDO::PARAM_STR);
		$update_new_tweet_sth->execute();
		if ( $update_new_tweet_sth->rowCount() == 0 ) {
			// continue;
			throw new Exception();
		}
	}
}
catch ( PDOException $e ) {

}
catch ( Exception $e ) {

}

try {
	$delete_past_log_query = "DELETE FROM toots WHERE pubDate < CURRENT_TIMESTAMP - INTERVAL '1 month'";
	$delete_past_log_sth = $pdo->query($delete_past_log_query);
}
catch ( PDOException $e ) {

}
catch ( Exception $e ) {
	
}
